from django.shortcuts import render
from django.http import HttpResponse, JsonResponse
from .serializer import get_url_store_pro,getProductsSerializer_for_endpoint,getstoredataSerializer, getProductsSerializer,getstoreidSerializer,get_url_store_cat
from rest_framework.views import APIView
from rest_framework import status
from rest_framework.response import Response
from background_task import background
import selenium
from selenium import webdriver
import datetime
import time
import json
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import TimeoutException
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver import ActionChains
import requests

from selenium.webdriver.support import expected_conditions as EC

# Create your views here.
def log(msg, logging = True):
    """
    Parameters
    ----------
    msg : (str)
        log message to be printed on the screen and logged with time in file logging.txt.
    logging : (boolean)
        a boolean argument (True/False) which default is True. Use it when verbose is a
        demand by printing scraping steps and failures (if any).
    Returns
    -------
    None
    """
    if logging == True:
        current_time = datetime.datetime.today().strftime('%Y-%m-%d_%H:%M:%S')
        logging_message = msg + " @" + current_time + "\n"
        print(logging_message)
        with open("logging.txt", 'a+') as logging:
            logging.write(logging_message)
def login(myBrowser):
    myBrowser.find_element_by_id("fm-login-id").send_keys("umair.ather84@gmail.com")
    time.sleep(2)
    myBrowser.find_element_by_id("fm-login-password").clear()
    time.sleep(2)
    myBrowser.find_element_by_id("fm-login-password").send_keys("paradigm_123")
    time.sleep(2)
    myBrowser.find_element_by_class_name("fm-button").click()
    time.sleep(1)
    '''
    try:
        myBrowser.find_element_by_class_name("fm-button").click()
    except:
        pass
        '''
def force_render(browser, logging = True):
    """AliExpress product page does not render completely unless it is scrolled up/down
    and to make sure all data points are vaisible to mainScraper this function zoom out
    from css content.
    Parameters
    ----------
    browser : (webdriver instance)
        Selenium webdriver instance
    logging : (boolean)
        A boolean argument (True/False) which default is True. Use it when verbose is a
        demand by printing scraping steps and failures (if any).
    Returns
    -------
    None
    """
    browser.maximize_window()
    browser.execute_script("window.scrollTo(0, 1080);")
    browser.execute_script("window.scrollTo(0, 0);")
    browser.find_element_by_tag_name("html").send_keys(Keys.CONTROL,Keys.SUBTRACT)
    browser.find_element_by_tag_name("html").send_keys(Keys.CONTROL,Keys.SUBTRACT)
    browser.find_element_by_tag_name("html").send_keys(Keys.CONTROL,Keys.SUBTRACT)
    browser.find_element_by_tag_name("html").send_keys(Keys.CONTROL,Keys.SUBTRACT)
    browser.find_element_by_tag_name("html").send_keys(Keys.CONTROL,Keys.SUBTRACT)
    log("Rendered all page", logging)

def get_element(browser, by, selector):
    """use WebDriverWait method to wait for 10 seconds for an element to load on the page
    Parameters
    ----------
    browser : (webdriver instance)
        Selenium webdriver instance to
    by : (selenium.webdriver.common.by.By  instance)
        Set of supported locator strategies that tells the borwser which strategy to use in
        finding an element (CLASS_NAME, XPATH, CSS_SELECTOR)
    selector : (str)
        A string representing the element selector from the DOM whichever xpath, class name
        or css selector.
    Returns
    -------
    (Webdrive element)
        A found element which can be processed later by procedures such as .click() and
        has attributes such as .text which represent the innerHTML text of an element.
    """
    try:
        element = WebDriverWait(browser, 20).until(EC.presence_of_element_located((by, selector)))
        return element
    except TimeoutException:
        return None

def get_elements(browser, by, selector):
    """use WebDriverWait method to wait for 10 seconds for an element to load on the page
    Parameters
    ----------
    browser : (webdriver instance)
        Selenium webdriver instance
    by : (selenium.webdriver.common.by.By  instance)
        Set of supported locator strategies that tells the borwser which strategy to use in
        finding an element (CLASS_NAME, XPATH, CSS_SELECTOR)
    selector : (str)
        A string representing the element selector from the DOM whichever xpath, class name
        or css selector.
    Returns
    -------
    (List of Webdrive element)
        A found elements which can be processed later by procedures such as .click() and
        has attributes such as .text which represent the innerHTML text of an element.
    """
    try:
        element = WebDriverWait(browser, 20).until(EC.presence_of_all_elements_located((by, selector)))
        return element
    except TimeoutException:
        return None

def get_product_page(browser, product_url, logging = True):
    """A function that opens the product url in the browser instance created previously
    and passed to the same function
    Parameters
    ----------
    browser : (webdriver instance)
        Selenium webdriver instance
    product_url : (str)
        The product page url.
    logging : (boolean)
        A boolean argument (True/False) which default is True. Use it when verbose is a
        demand by printing scraping steps and failures (if any).
    Returns
    -------
    None
        It returns None, however it brings up the browser instance to the product url
        to be ready for scraping.
    """
    log(f"Got product {product_url} page", logging)
    disable_modal_ad(browser, logging)
    force_render(browser, logging)
    by = By.CSS_SELECTOR
    selector = ".result-info"
    selector1 = ".ng-switcher"
    WebDriverWait(browser, 20).until(EC.element_to_be_clickable(
        (By.XPATH, "//a[contains(@class, 'switcher-info')]/span[@class='ship-to']/i"))).click()
    # WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.XPATH,
    #                                                            "//a[@class='address-select-trigger']//span[@class='css_flag css_in']//span[@class='shipping-text']"))).click()
    # WebDriverWait(browser, 10).until(EC.element_to_be_clickable((By.XPATH,
    #                                                            "//li[@class='address-select-item ']//span[@class='shipping-text' and text()='Afghanistan']"))).click()
    time.sleep(5)
    browser.find_element_by_class_name('address-select-trigger').click()
    browser.find_element_by_class_name("filter-input").send_keys("United kingdom")
    html_list = browser.find_element_by_class_name("address-select-content")
    items = html_list.find_elements_by_tag_name("li")
    time.sleep(5)
    items[222].click()
    browser.find_element_by_class_name('ui-button').click()


def disable_modal_ad(browser, logging = True):
    """A function that check/disable the modal ad which covers the dom and prevent the
    script from interacting with the webpage
    """
    by = By.CLASS_NAME
    selector = 'next-dialog-close'
    modal_ad = get_element(browser, by = by, selector = selector)
    if modal_ad:
        modal_ad.click()
        log("Disabled modal ad", logging)
    else: log("No modal ad encountered", logging)

def get_store_name(browser, logging = True):
    """A function that search for the store name in the product page and return
    it
    """
    store_name = browser.find_element_by_id("hd").text.split("\n")[0]
    if store_name:
        log(f"Got store name: {store_name}", logging)
        return store_name
    else:
        log("Could not get store name", logging)
        return None


def get_number_of_products(browser, logging = True):
    """A function that search for the shipping cost in the product page and return
    it. This value depend on the country of scraping device.
    """
    by = By.CSS_SELECTOR
    selector = ".result-info"

    shipping_cost = get_element(browser, by = by, selector = selector)
    #like = browser.find_elements_by_class_name('ng-item')
    #likes = like[5].click()
    #select_con = browser.find_elements_by_class_name('address-select-trigger')
    #select_con[0].click()

    #c = browser.find_elements_by_class_name('address-select-item')
    #d = browser.find_element_by_xpath("//li[contains(@data-name, 'Andorra')]")
    #d.select()
    if shipping_cost:
        shipping_cost = shipping_cost.text.strip('Shipping: ')
        log(f"Got shipping cost: {shipping_cost}", logging)
        return shipping_cost
    else:
        log("Could not get shipping cost", logging)
        return None


def get_Category_list(browser, logging = True):
    """A function that search for the store name in the product page and return
    it
    """
    listc = browser.find_elements_by_xpath("//div[contains(@class, 'j-module')]")[0].text.split("\n")
    ctgry = []
    i = 1
    len = listc.__len__()
    while i < len:
        try:
            time.sleep(2)
            listc = browser.find_elements_by_xpath("//div[contains(@class, 'j-module')]")[0].text.split("\n")
            time.sleep(2)
            browser.find_element_by_link_text(listc[i]).click()
            time.sleep(1)
            browser.switch_to_window(browser.window_handles[-1])
            if 'login.aliexpress.com' in browser.current_url:
                login(browser)
            listp = (get_number_of_products(browser, logging).split(" ")[0]).split(",")
            pr = ""
            for p in listp:
                pr = pr + p
            pr = int(pr)
            ctg = {
                "categoryName": listc[i],
                "categorySlug": "",
                "totalProducts": pr,
                "categoryurl" : browser.current_url
            }
            ctgry.append(ctg)

            i = i + 1
        except:
            print("Error while click line 249")
    return ctgry

def mainScraper(browser, url, logging):
    """A function that combine all data points and scrape them at once returning one python dictionary
    including all available data points from the product page
    Parameters
    ----------
    browser : (webdriver instance)
        Selenium webdriver instance
    url : (str)
        The product page url.
    logging : (boolean)
        A boolean argument (True/False) which default is True. Use it when verbose is a
        demand by printing scraping steps and failures (if any).
    Returns
    -------
    (dict)
        A python dictionary that include all data points structured as per PRODUCT_DATA dictionary below
    """
    get_product_page(browser, url, logging)

    listp = (get_number_of_products(browser, logging).split(" ")[0]).split(",")
    pr = ""
    for p in listp:
        pr = pr + p
    pr = int(pr)
    ctgry = get_Category_list(browser, logging)
    PRODUCT_DATA = {
        "totalProducts": pr,
        "storeTitle":get_store_name(browser, logging),
        "categories":ctgry
    }

    return PRODUCT_DATA
def set_proxy(chrome_options,st):
    f = open("proxy.txt", "r")

    lines = f.readlines()
    for line in lines:
        PROXY = line # HOST:PORT
        PROXY = PROXY.replace("\n", "")
        chrome_options.add_argument('--proxy-server=%s' % PROXY)
        chrome_options.add_argument("ignore-certificate-errors")
        # chrome_options.binary_location = os.environ.get("GOOGLE_CHROME_BIN")
        myBrowser = webdriver.Chrome(
            executable_path="templates/chromedriver", chrome_options=chrome_options)
        from selenium.common.exceptions import TimeoutException
        from selenium.webdriver.support.ui import WebDriverWait
        from selenium.webdriver.support import expected_conditions as EC
        from selenium.webdriver.common.by import By
        timeout = 0.1
        try:
            #url = "https://sdwk.aliexpress.com/store/all-wholesale-products/" + st + ".html"
            myBrowser.get("https://login.aliexpress.com")
            #element_present = EC.presence_of_element_located((By.ID, 'fm-login-id'))
            #WebDriverWait(myBrowser, timeout).until(element_present)
            login(myBrowser)

        except TimeoutException:
            print("Timed out waiting for page to load")
            myBrowser.quit()
class getstoredata(APIView):

    serializer_class = getstoredataSerializer

    def post(self, request):
        print(datetime.datetime.now())
        storeid = request.POST.get('storeid')
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        #myBrowser = set_proxy(chrome_options,storeid)
        myBrowser = webdriver.Chrome(
            executable_path="/usr/bin/chromedriver", chrome_options=chrome_options)
        myBrowser.delete_all_cookies()
        myBrowser.get("https://login.aliexpress.com")
        # element_present = EC.presence_of_element_located((By.ID, 'fm-login-id'))
        # WebDriverWait(myBrowser, timeout).until(element_present)
        login(myBrowser)

        url = "https://aliexpress.com/store/all-wholesale-products/" + storeid + ".html"
        myBrowser.get(url)
        data = mainScraper(myBrowser, url, logging=True)

        data = {'success': True ,
                    'store_id':storeid,
                    'data': data
                    }
        print(datetime.datetime.now())
        storeid = storeid + ".txt"
        with open(storeid, 'w') as outfile:
            json.dump(data, outfile)
        return JsonResponse(data=data, status=status.HTTP_201_CREATED)
def get_Prodcut_list(browser, Noofproduct):

    products = []
    count = 0
    time.sleep(2)
    pglen = browser.find_elements_by_xpath("//div[contains(@class, 'ui-pagination')]")[1].text.split("\n").__len__()-2
    for i in range(0, pglen):
        if i > 0:
            time.sleep(1)
            browser.find_elements_by_xpath("//a[contains(@class, 'ui-pagination-next')]")[0].click()
            time.sleep(2)
        data = browser.find_elements_by_xpath("//a[contains(@class, 'pic-rind')]")
        for i in range(0, data.__len__()):
            try:
                data = browser.find_elements_by_xpath("//a[contains(@class, 'pic-rind')]")
                data[i].click()
                time.sleep(4)
                title  = browser.find_elements_by_xpath("//div[contains(@class, 'product-title')]")[0].text
                original = browser.find_elements_by_xpath("//div[contains(@class, 'product-price-del')]")
                if original != []:
                    original = browser.find_elements_by_xpath("//div[contains(@class, 'product-price-del')]")[0].text.split("￡")[1]
                sale = browser.find_elements_by_xpath("//div[contains(@class, 'product-price-current')]")[0].text.split("￡")[1]
                reviews =browser.find_elements_by_xpath("//span[contains(@class, 'product-reviewer-reviews black-link')]")
                if reviews != []:
                    reviews = int(browser.find_elements_by_xpath("//span[contains(@class, 'product-reviewer-reviews black-link')]")[0].text.split(" ")[0])
                rating = browser.find_elements_by_xpath("//span[contains(@class, 'overview-rating-average')]")
                if rating != []:
                    rating = browser.find_elements_by_xpath("//span[contains(@class, 'overview-rating-average')]")[0].text
                colors = browser.find_elements_by_xpath("//div[contains(@class, 'sku-property-image')]")
                variants = []
                coulrs = {
                    "type": "color",
                    "options": []
                }
                variants.append(coulrs)
                for color in colors:
                    opt = {
                        "code" : color.find_element_by_tag_name("img").get_attribute("title"),
                        "image_url" : color.find_element_by_tag_name("img").get_attribute("src")
                        }
                    coulrs["options"].append(opt)
                imgs = browser.find_elements_by_xpath("//div[contains(@class, 'images-view-item')]")
                main_disc = {
                    "image_urls":[],
                    "text": browser.find_elements_by_xpath("//div[contains(@class, 'product-title')]")[0].text
                }
                for img in imgs:
                    main_disc["image_urls"].append(img.find_element_by_tag_name("img").get_attribute("src"))
                product = {
                    "product_url": browser.current_url,
                    "title" : title,
                    "reg_price": original,
                    "sale_price": sale,
                    "total_reviews": reviews,
                    "avg_rating": rating,
                    "variants": variants,
                    "main_description": main_disc

                }
                products.append(product)
                count = count + 1
                if count == Noofproduct:
                    return products
                browser.execute_script("window.history.go(-1)")
                time.sleep(2)
            except:
                print("Error in line 347")
    return products
def mainScraper2(browser, url,Noofproduct , logging):
    """A function that combine all data points and scrape them at once returning one python dictionary
    including all available data points from the product page
    Parameters
    ----------
    browser : (webdriver instance)
        Selenium webdriver instance
    url : (str)
        The product page url.
    logging : (boolean)
        A boolean argument (True/False) which default is True. Use it when verbose is a
        demand by printing scraping steps and failures (if any).
    Returns
    -------
    (dict)
        A python dictionary that include all data points structured as per PRODUCT_DATA dictionary below
    """
    browser.get(url)
    get_product_page(browser, url,  logging)
    products = get_Prodcut_list(browser, Noofproduct)

    PRODUCT_DATA = {
        "success": "true",
        "products": products
    }

    return PRODUCT_DATA
class getproducts(APIView):

    serializer_class = getProductsSerializer

    def post(self, request):
        url = request.POST.get('categoryurl')
        Noofproduct = int(request.POST.get('Noofproducts'))
        offset = request.POST.get('offset')
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")

        myBrowser = webdriver.Chrome(
            executable_path="/usr/bin/chromedriver", chrome_options=chrome_options)
        myBrowser.get("https://login.aliexpress.com/")
        login(myBrowser)
        myBrowser.get(url)
        data = mainScraper2(myBrowser, url, Noofproduct, logging=True)

        data = {'success': True ,
                    'data': data
                    }
        return JsonResponse(data=data, status=status.HTTP_201_CREATED)



@background(schedule=5)
def Scrape_cat_product(url,noP,oo,cat_name,storei):
    url = url
    Noofproduct = 100
    offset = 1
    storeid = store_id
    name = storei +cat_name+".txt"
    try:
        with open(name, 'r+') as file:
            return "Already " + name
    except:
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")

        myBrowser = webdriver.Chrome(
            executable_path="/usr/bin/chromedriver", chrome_options=chrome_options)
        myBrowser.get("https://login.aliexpress.com/")
        login(myBrowser)
        myBrowser.get(url)
        data = mainScraper2(myBrowser, url, Noofproduct, logging=True)

        data = {'success': True,
                'name': url,
                'data': data
                }
        myBrowser.quit()
        name = storei+cat_name+".txt"
        with open(name, 'w') as outfile:
            json.dump(data, outfile)



        return JsonResponse(data={"data": "Filescreated"}, status=status.HTTP_201_CREATED)


@background(schedule=5)
def Scrap_store_category(store_id):
    print(datetime.datetime.now())
    storeid = store_id
    name = storeid + ".txt"
    try:
        with open(name, 'r+') as file:
                return "Already "+name
    except:
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument("--headless")
        chrome_options.add_argument("--disable-dev-shm-usage")
        chrome_options.add_argument("--no-sandbox")
        # myBrowser = set_proxy(chrome_options,storeid)
        myBrowser = webdriver.Chrome(
            executable_path="/usr/bin/chromedriver", chrome_options=chrome_options)
        myBrowser.delete_all_cookies()
        myBrowser.get("https://login.aliexpress.com")
        # element_present = EC.presence_of_element_located((By.ID, 'fm-login-id'))
        # WebDriverWait(myBrowser, timeout).until(element_present)
        login(myBrowser)

        url = "https://aliexpress.com/store/all-wholesale-products/" + storeid + ".html"
        myBrowser.get(url)
        data = mainScraper(myBrowser, url, logging=True)

        data = {'success': True,
                'store_id': storeid,
                'data': data
                }
        print(datetime.datetime.now())
        storeid = storeid + ".txt"
        with open(storeid, 'w') as outfile:
            json.dump(data, outfile)
        return JsonResponse(data=data, status=status.HTTP_201_CREATED)




class category_id(APIView):

    serializer_class = getProductsSerializer_for_endpoint

    def post(self, request):
        url = request.data['categoryurl']
        cat_name = request.data['categorname']
        store_i = request.data['store_id']
        cat_name = cat_name.replace(" ","")
        cat_name = cat_name.replace(".", "")
        cat_name = cat_name.replace("'", "")
        cat_name = cat_name.replace(",", "")
        cat_name = cat_name.replace("-", "")
        name = store_i+cat_name+".txt"

        try:
            with open(name) as json_file:
                dat = json.load(json_file)
                return JsonResponse(data=dat ,status=status.HTTP_200_OK)
        except:
            try:
                name_check = store_i+cat_name
                with open("store_cat.txt", 'r+') as file:
                    for line in file:
                        line = line.strip()  # preprocess line
                        if line == name_check:
                            return Response(status=status.HTTP_208_ALREADY_REPORTED)
            except:
                f = open("store_cat.txt", "a+")
                f.write(store_i + cat_name+"\n")
                f.close()
            Scrape_cat_product(url,10,1,cat_name,store_i,verbose_name="scrape_cat_product")
            return Response(status=status.HTTP_202_ACCEPTED)


class store_id(APIView):

    serializer_class = getstoreidSerializer

    def post(self, request):
        store_id = request.data['storeid']
        name  = store_id+".txt"
        try:
            with open(name) as json_file:
                dat = json.load(json_file)
                return JsonResponse(data=dat ,status=status.HTTP_200_OK)
        except:
            with open("store_ids.txt", 'r+') as file:
                for line in file:
                    line = line.strip()  # preprocess line
                    if line == store_id:
                        return Response(status=status.HTTP_208_ALREADY_REPORTED)

            f = open("store_ids.txt", "a+")
            f.write(str(store_id)+"\n")
            f.close()
            Scrap_store_category(store_id)
            return Response(status=status.HTTP_202_ACCEPTED)

    def get(self,request):
        # Sample 3 - a more pythonic way with efficient memory usage. Proper usage of with and file iterators.
        lines = []
        with open("store_ids.txt",'r+') as file:
            for line in file:
                line = line.strip()  # preprocess line
                lines.append(line)  # take action on line instead of storing in a list. more memory efficient at the cost of execution speed.
        data = dict()
        data = {'data':lines}
        return Response(lines,status=status.HTTP_200_OK)



@background(schedule=5)
def notify_user():
    print("I am schedule\n")

def get_data_from_server(request):
    r = requests.get('http://0.0.0.0:5055/store_id/', params=request.GET)
    t = r.text
    t = t.replace("[","")
    t = t.replace("]","")
    t = t.replace("\"","")
    s = int(t)
    #f = open("store_ids.txt", "a+")
    #f.write(str(t) + "\n")
    #f.close()
    x = requests.post('http://0.0.0.0:5055/getstoredata/', data={'storeid':s})
    print(x)
    return JsonResponse({'data':t},status=status.HTTP_200_OK)
def send_store_cat_json(request):
    lines = []
    with open("store_ids.txt", 'r+') as file:
        for line in file:
            line = line.strip()  # preprocess line
            lines.append(line)  # take action on line instead of storing in a list. more memory efficient at the cost of execution speed.

    for line in lines:
        l = str(line)+".txt"
        with open(l) as json_file:
            dat = json.load(json_file)
            ll = requests.post('https://pay-load.herokuapp.com/sjson/', json=dat)
            print(dat)
    return JsonResponse({'hi':'hi'})
class get_store_cat_json(APIView):
    serializer_class = get_url_store_cat
    def post(self, request):
        try:
            #name = request.POST.get('store_id')
            name = request.data['store_id']
            #l = name + ".txt"
            l = str(name)+".txt"
            with open(l) as json_file:
                dat = json.load(json_file)
                return JsonResponse(data=dat ,status=status.HTTP_200_OK)
        except:
            return JsonResponse(data= {'message':"File errors"},status=status.HTTP_400_BAD_REQUEST)

class save_store_cat_json(APIView):
    def post(self, request):
        try:
            file_name = request.data['name']
            storeid = file_name + ".txt"
            with open(storeid, 'w') as outfile:
                json.dump(request.data, outfile)
            return JsonResponse(data= {'message':"File saved"},status=status.HTTP_200_OK)
        except:
            return JsonResponse(data= {'message':"File errors"},status=status.HTTP_400_BAD_REQUEST)
class get_products_for_category(APIView):
    def get(self,request):
        lines = []
        with open("store_ids.txt", 'r+') as file:
            for line in file:
                line = line.strip()  # preprocess line
                lines.append(line)  # take action on line instead of storing in a list. more memory efficient at the cost of execution speed.

        for line in lines:
            l = str(line) + ".txt"

            with open(l) as json_file:
                dat = json.load(json_file)
                print(dat)
                for cat_l in dat['data']['categories']:
                    print(datetime.datetime.now())
                    cat_file_name = str(line) + cat_l['categoryName'].replace(" ","")
                    #url =requests.post('http://0.0.0.0:5055/getproduct/', data={'categoryurl':cat_l['categoryurl'],'Noofproducts':1,'offset':1})
                    url = cat_l['categoryurl']
                    Noofproduct = 100
                    offset = 1

                    chrome_options = webdriver.ChromeOptions()
                    chrome_options.add_argument("--headless")
                    chrome_options.add_argument("--disable-dev-shm-usage")
                    chrome_options.add_argument("--no-sandbox")

                    myBrowser = webdriver.Chrome(
                        executable_path="templates/chromedriver", chrome_options=chrome_options)
                    myBrowser.get("https://login.aliexpress.com/")
                    login(myBrowser)
                    myBrowser.get(url)
                    data = mainScraper2(myBrowser, url, Noofproduct, logging=True)

                    data = {'success': True,
                            'name': cat_file_name,
                            'data': data
                            }

                    with open(cat_file_name, 'w') as outfile:
                        json.dump(data, outfile)
                    myBrowser.quit()
        return JsonResponse(data={"data":"Filescreated"}, status=status.HTTP_201_CREATED)

def send_json(request):
    import glob
    m = glob.glob('./*.txt')
    for l in m:
        try:
            with open(l[2:]) as json_file:
                dat = json.load(json_file)
                ll = requests.post('https://pay-load.herokuapp.com/sjson/', json=dat)
                print(ll)
                print(dat)
        except:
            print("did not send "+l)
    return HttpResponse(content={"OK"})


class get_store_pro_json(APIView):
    serializer_class = get_url_store_pro
    def post(self, request):
        try:
            #name = request.POST.get('store_id')
            name = request.data['store_id']
            cat_name = request.data['categorname']
            cat_name = cat_name.replace(" ", "")
            cat_name = cat_name.replace(".", "")
            cat_name = cat_name.replace("'", "")
            cat_name = cat_name.replace(",", "")
            cat_name = cat_name.replace("-", "")
            name = name+cat_name
            #l = name + ".txt"
            l = str(name)+".txt"
            with open(l) as json_file:
                dat = json.load(json_file)
                return JsonResponse(data=dat ,status=status.HTTP_200_OK)
        except:
            return JsonResponse(data= {'message':"File errors"},status=status.HTTP_400_BAD_REQUEST)
