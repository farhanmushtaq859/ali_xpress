from rest_framework import serializers


class getstoredataSerializer(serializers.Serializer):

    storeid =  serializers.CharField(max_length=100)

    class Meta:
        field = "__all__"
class getProductsSerializer(serializers.Serializer):

    categoryurl =  serializers.CharField(max_length=5000)
    Noofproducts = serializers.IntegerField()
    offset = serializers.CharField(max_length=200)

    class Meta:
        field = "__all__"

class getProductsSerializer_for_endpoint(serializers.Serializer):

    categoryurl =  serializers.CharField(max_length=5000)
    categorname = serializers.CharField(max_length=1000)
    store_id = serializers.CharField(max_length=100)

    class Meta:
        field = "__all__"

class getstoreidSerializer(serializers.Serializer):

    storeid =  serializers.CharField(max_length=100)

    class Meta:
        field = "__all__"

class get_url_store_cat(serializers.Serializer):

    store_id =  serializers.CharField(max_length=5000)

    class Meta:
        field = "__all__"

class get_url_store_pro(serializers.Serializer):

    store_id =  serializers.CharField(max_length=5000)
    categorname = serializers.CharField(max_length=5000)

    class Meta:
        field = "__all__"
