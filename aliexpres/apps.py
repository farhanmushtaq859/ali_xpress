from django.apps import AppConfig


class AliexpresConfig(AppConfig):
    name = 'aliexpres'
