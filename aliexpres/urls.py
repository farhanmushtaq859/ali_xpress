# accounts/urls.py
from django.urls import path

from .views import *
from django.conf.urls import url
from django.views.generic.base import TemplateView
urlpatterns = [
    path('getstoredata/', getstoredata.as_view()),
    path('getproduct/', getproducts.as_view()),
    path('store_id/', store_id.as_view()),
    path('category_id/', category_id.as_view()),

    path('getdata/', get_data_from_server,name="getdata"),
    path('sentjson/', send_store_cat_json, name="sentdata"),
    path('get_cat_json/', get_store_cat_json.as_view()),
    path('get_pro_json/', get_store_pro_json.as_view()),
    path('sjson/', save_store_cat_json.as_view()),
    path('scrpcat/', get_products_for_category.as_view()),
    path('sendjsonall/', send_json,name="sendjsonall"),

]